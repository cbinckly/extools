##################
Tools for Extender
##################

-------------------------------------------------
A library of utilities for working with Extender.
-------------------------------------------------

.. image:: https://readthedocs.org/projects/extools/badge/?version=latest
    :target: https://extools.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

The ``extools`` package provides a number of functions and classes
to make it easier to write clear, concise, Python code in 
scripts and modules for Python Extender.  

``extools`` is an `open source`_ project maintained by `2665093 Ontario Inc`_.
Everyone is invited to `file a bug`_, `review the source`_,
or `contribute`_.

.. _open source: https://bitbucket.org/cbinckly/extools/src/master/LICENSE.txt
.. _2665093 Ontario Inc: https://2665093.ca
.. _contribute: https://bitbucket.org/cbinckly/extools/src/master/CONTRIBUTING.txt
.. _file a bug: https://bitbucket.org/cbinckly/extools/issues/new
.. _review the source: https://bitbucket.org/cbinckly/extools/

The `API documentation`_ is available on readthedocs.io.

.. _API documentation: https://extools.readthedocs.io

Installation
************

To be available from Extender scripts, the ``extools`` package must be
installed in the embedded Extender Python environment.  This is not 
trivial.  

The use of the `Python Package Manager for Orchid Extender`_ is recommended.

.. _Python Package Manager for Orchid Extender: https://2665093.ca/

Included Functions and Classes
******************************

Full api documentation for the package is available on readthedocs.io.

License
*******

The ``extools`` package is distributed under the GNU GPL version 3 copyleft
license.  You must read the license in full before modifying, redistributing,
or using the code in a derivative work.  In short:

    | You may copy, distribute and modify the software as long as you track 
      changes/dates in source files. Any modifications to or software including
      (via compiler) GPL-licensed code must also be made available under the 
      GPL along with build & install instructions.[1]_

.. [1] from https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)

