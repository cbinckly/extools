extools.view.utils
-------------------

Utilities for working with views and data.

.. automodule:: extools.view.utils.customer
    :members: 
    :show-inheritance:

.. automodule:: extools.view.utils.glaccount
    :members: 
    :show-inheritance:

.. automodule:: extools.view.utils.iet
    :members: PATYPES, TTYPES, partner_document_for
    :show-inheritance:

.. automodule:: extools.view.utils.item
    :members: 
    :show-inheritance:

.. automodule:: extools.view.utils.order
    :members: 
    :show-inheritance:

.. automodule:: extools.view.utils.shipment
    :members: 
    :show-inheritance:

.. automodule:: extools.view.utils.units
    :members: 
    :show-inheritance:
