extools tests
-------------

.. automodule:: extools.tests.test_extools
    :members:
    :show-inheritance:

.. automodule:: extools.tests.extest_extools
    :members:
    :show-inheritance:
