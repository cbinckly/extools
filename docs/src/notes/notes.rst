Notes
=====

.. toctree::
   :hidden:
   :maxdepth: 4

   custom_table_fields 


Some collected notes on working with Extender.  First up: field types.
Easily determined by building a table in the Custom Table tool and 
exporting the module.
